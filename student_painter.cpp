//
// Created by hamsatom on 15.05.2017.
//

#include <cmath>
#include <iostream>
#include <thread>
#include <chrono>
#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <ctime>
#include <unistd.h>
#include <complex>

#include "mzapo_parlcd.h"
#include "mzapo_regs.h"
#include "mzapo_phys.h"
#include "student_painter.h"

const int WIDTH = 480;
const int HEIGHT = 320;
const long double JULIA_LIMIT = 4.0;
const long double ITERATIONS_LIMIT = 200.0;
const long double IMAGINARY_START = (JULIA_LIMIT * HEIGHT) / (WIDTH * -2.0);
const long double REAL_STEP = JULIA_LIMIT / WIDTH;
const long double IMAGINARY_STEP = (-2.0 * IMAGINARY_START) / HEIGHT;
long double previousX = 999.9, previousY = 999.9, previousImag = 999.9;

/**
 * Inspired by http://lodev.org/cgtutor/juliamandelbrot.html and http://stackoverflow.com/questions/33978167/julia-set-rendering-code
 * @param positionX
 * @param positionY
 * 
 * @param constantReal
 * @param constantImaginary
 * @return
 */
void get_julia_set(unsigned int julia_set[WIDTH * HEIGHT],
                   long double *positionX,
                   long double *positionY,
                   long double *constantReal,
                   long double *constantImaginary) {

  if (previousX == *positionX && previousY == *positionY && previousImag == *constantImaginary) {
    return;
  }

  std::cout << "X=" << *positionX << ", Y=" << *positionY << ", C_real=" << *constantReal << ", C_imaginary="
            << *constantImaginary << std::endl;

  long double previous_imaginary, previous_real, new_imaginary, new_real, succesfull_iterations, real_part_squared,
      imaginary_part_squared, ratio;
  unsigned char red, green, blue;

  previousX = *positionX;
  previousY = *positionY;
  previousImag = *constantImaginary;

  *positionY += -2.0;
  *positionX += IMAGINARY_START;

  for (int i = 0; i < HEIGHT; i++) {
    previous_imaginary = i * IMAGINARY_STEP + *positionX;
    for (int j = 0; j < WIDTH; j++) {
      previous_real = j * REAL_STEP + *positionY;
      new_real = previous_real;
      new_imaginary = previous_imaginary;

      for (succesfull_iterations = 0.0; succesfull_iterations < ITERATIONS_LIMIT; ++succesfull_iterations) {
        // Cannot use std::pow because it round the numbers in a weird way
        real_part_squared = new_real * new_real;
        imaginary_part_squared = new_imaginary * new_imaginary;
        if (real_part_squared + imaginary_part_squared > JULIA_LIMIT) {
          break;
        }
        new_imaginary = 2.0 * new_real * new_imaginary + *constantImaginary;
        new_real = real_part_squared - imaginary_part_squared + *constantReal;
      }

      // Calculation according to https://cw.fel.cvut.cz/wiki/courses/b3b36prg/semestral-project/start
      ratio = succesfull_iterations / ITERATIONS_LIMIT;
      red = (unsigned char) 9.0 * (1.0 - ratio) * ratio * ratio * ratio * 255.0;
      green = (unsigned char) 15.0 * (1.0 - ratio) * (1.0 - ratio) * ratio * ratio * 255.0;
      blue = (unsigned char) 8.5 * (1.0 - ratio) * (1.0 - ratio) * (1.0 - ratio) * ratio * 255.0;
      julia_set[j + i * WIDTH] = ((red & 0xf8) << 8) | ((green & 0xfc) << 3) | ((blue & 0xf8) >> 3);
    }
  }

  *positionY -= -2.0;
  *positionX -= IMAGINARY_START;
}

void draw(long double *positionX,
          long double *positionY,
          long double *constantReal,
          long double *constantImaginary,
          std::mutex *mutex,
          bool *finished) {

  unsigned int julia_set[HEIGHT * WIDTH];
  unsigned char *parlcd_mem_base;
  parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  parlcd_hx8357_init(parlcd_mem_base);

  std::unique_lock <std::mutex> thread_lock(*mutex);
  thread_lock.unlock();
  while (!*finished) {
    thread_lock.lock();
    get_julia_set(julia_set, positionX, positionY, constantReal, constantImaginary);
    drawdisplay(julia_set, parlcd_mem_base);
    thread_lock.unlock();
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
  }
}