//
// Created by hamsatom on 15.05.2017.
//

#include <thread>
#include <mutex>
#include <iostream>
#include <string>

#include "student_painter.h"
#include "student_knobs_handler.h"
#include "student_presenter.h"

const std::string presentation_option = "-present";

int main(int argc, char *argv[]) {
  bool presentation_mode = argc > 1 && (presentation_option.compare(argv[1]) == 0);

  long double positionX = 0.0;
  if (argc > 1 && !presentation_mode) {
    positionX = strtold(argv[1], NULL);
  }

  long double positionY = 0.0;
  if (argc > 2) {
    positionY = strtold(argv[2], NULL);
  }

  long double constantReal = -0.678;
  if (argc > 3) {
    constantReal = strtold(argv[3], NULL);
  }

  long double constantImaginary = 0.3125;
  if (argc > 4) {
    constantImaginary = strtold(argv[4], NULL);
  }

  std::cout << "X=" << positionX << ", Y=" << positionY << ", C_real=" << constantReal << ", C_imaginary="
            << constantImaginary << std::endl;

  bool finished = false;
  std::mutex mutex;
  std::thread knobs_thread;
  if (presentation_mode) {
    knobs_thread = std::thread(present, &constantImaginary, &mutex, &finished);
  } else {
    knobs_thread = std::thread(handle_knobs, &positionX, &positionY, &constantImaginary, &mutex, &finished);
  }
  std::thread drawing_thread(draw, &positionX, &positionY, &constantReal, &constantImaginary, &mutex, &finished);

  // wait for input
  getchar();

  finished = true;

  drawing_thread.join();
  knobs_thread.join();
}