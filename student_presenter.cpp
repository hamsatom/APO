//
// Created by hamsatom on 5/17/17.
//
#include <thread>
#include <chrono>

#include "student_presenter.h"

const int sleep_time = 51;

void present(long double *constantImaginary, std::mutex *mutex, bool *finished) {
  std::unique_lock <std::mutex> thread_lock(*mutex);
  thread_lock.unlock();
  while (!*finished) {
    for (long double i = 0.28; i < 0.5 && !*finished; i += 0.001) {
      thread_lock.lock();
      *constantImaginary = i;
      thread_lock.unlock();
      std::this_thread::sleep_for(std::chrono::milliseconds(sleep_time));
    }
    for (long double i = 0.5; i >= 0.28 && !*finished; i -= 0.001) {
      thread_lock.lock();
      *constantImaginary = i;
      thread_lock.unlock();
      std::this_thread::sleep_for(std::chrono::milliseconds(sleep_time));
    }
  }
}
