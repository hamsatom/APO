//
// Created by hamsatom on 15.05.2017.
//

#pragma once
#include <mutex>

void handle_knobs(long double *positionX,
                  long double *positionY,
                  long double *constantImaginary,
                  std::mutex *mutex,
                  bool *finished);
