**Autor:** Tomáš Hamsa  
**Projekt:** Výpočet a zobrazení Julia set  
**Předmět:** A0B36APO - Architektura počítačů  
**Semestr:** B162 - letní 2016/2017  
           

Úvod
====

Tento projekt vznikl v rámci semestrální práce na předmět APO. Jeho
cílem je vykreslit Julia set na obrazovku desky MicroZed. Julia set
neboli Juliova množina je množina všech bodů z v komplexní
rovině, pro které posloupnost z_{n+1}=z_{n}^2+c, kde z je počáteční
komplexní číslo a c je libovolné komplexní číslo, nediverguje. Hranice
takovéto množiny tvoří fraktál.


Instalace
=========

K provedení instalace bude potřeba stáhnout zdrojový kód z GITu,
zkompilovat ho a spustit na MicroZed.  

#### Potřebný software pro spuštění programu:

-   GIT klient - dostupný na <https://git-scm.com/downloads>

-   Arm linux C a C++ kompilátor minimální podporovaný verze C - C99 a
    C++ - C++11  
    dostupný na
    <https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads>

#### Kroky instalace:

1.  Otevřít konzoli v GIT klientovi

2.  Naklonovat projekt příkazem: git clone
    git@gitlab.fel.cvut.cz:hamsatom/APO.git

3.  Ve složce projektu (APO) zkompilovat projekt příkazem:  
    make TARGET  _IP=192.168.202.[poslední trojčíslí IP desky] run  

Pokud vše dopadlo úspěšně na desce by měla být zobrazena Juliova množina
uprostřed displeje s počátečním komplexním číslem C=-0.678, 0.3125

Ovládání
========

U zobrazeného fraktálu lze měnit jeho pozici prvníma dvěma tlačítky a
imaginární složku komplexního čísla C posledním, třetím tlačítkem.
Aplikaci lze spustit v prezentačním režimu přepínačem -present. Aplikaci
lze zastavit stisknutím jakékoliv klávesy na klávesnici.  

### Posun

**První - červené, tlačítko** posouvá Juliovu množinu po x ose, tedy
doleva nebo doprava. Pokud by hodnota byla mimo hranice displeje,
zobrazí se množina na druhé straně displeje, jako by displej objela.  
**Druhé - zelené, tlačítko** posouvá Juliovou množinou po Y ose, tedy
nahoru a dolu. Také platí, že při velkých nebo malých hodnotách se
množina přesune na opačnou stranu displeje, jako by obrazovku podjela.  
**Třetí - modré, tlačítko** mění imaginární složku čísla C, tedy vzhled
Juliovy množiny. Vizuálně nejzajímavější hodnoty jsou mezi hodnotami
0.3-0.57

### Presentační mód

Aplikaci lze spustit v prezentačním módu, kdy se hodnoty mění samy a
nereaguje na hodnoty na tlačítkách. Stačí aplikaci spustit s přepínáčem
-present, tedy konkrétněji:  
./app -present  
V presentačním módu se mění hodnota imaginární části C po krocích 0.001
od 0.3 do 0.57 a pak zpět.

#### Ukončení

Aplikaci lze kdykoliv za běhu ukončit z terminálu zmáčknutím jakékoliv
klávesy.